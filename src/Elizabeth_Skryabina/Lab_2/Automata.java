public class Automata {
    public STATES state;
    int cash;
    String menu[] = {"Эспрессо", "Черный кофе", "Макиато", "Кофе со сливками", "Капучино", "Мокка", "Супер мокка",
                     "Сливочный мокка", "Чай", "Горячий шоколад", "Двойной шоколад", "Шоколад на молоке"};
    int prices[] = {28, 30, 30, 34, 35, 30, 35, 34, 20, 26, 35, 20};

    Automata() {
        state = STATES.OFF;
        cash = 0;
    }

    void on() { //включение
        state = STATES.WAIT;
    }

    void off() { //выключение
        state = STATES.OFF;
    }

    void coin(int money) { //занесение денег на счет
        if (state == STATES.WAIT | state == STATES.ACCEPT) {
            state = STATES.ACCEPT;
            if (money==1 | money==2 | money==5 | money==10 | money==50 | money==100)
                cash += money;
            else {
                System.out.println("Автомат принимает: 1, 2, 5, 10, 50, 100");
                state = STATES.WAIT;
            }
        }
        else
            state = STATES.OFF;
        System.out.println("Cash = " + cash);
    }

    void printmenu() {
        for(int i=0; i<menu.length; i++) {
            System.out.print(menu[i] + " - ");
            System.out.println(prices[i]);
        }
    }

    void printState(){
        System.out.println(state);
    }

    void choice (String drink) { //выбор напитка
        if(state == STATES.ACCEPT) {
            int i;
            state = STATES.CHECK;
            for(i=0; i<menu.length-1; i++)
                if(drink == menu[i]) break;
            if(drink == menu[i])
                if(check(prices[i]))
                    cook(i);
                else canсel("Нехватает денег! У вас " + cash + "! Необходимо " + prices[i]);
            else canсel("Напиток не существует!");
        }
        else state = STATES.OFF;
    }

    boolean check(int price) { //проверка денег
        if(cash >= price) return true;
        else return false;
    }

    void cook(int i) { //приготовление напитка
        if (state == STATES.CHECK) {
            state = STATES.COOK;
            System.out.println("Готовится");
            printState();
            finish(i);
        }
        else state = STATES.OFF;
    }

    void canсel(String message) { //отмена
        if (cash > 0) {
            System.out.println(message);
            cash = 0;
        }
        state = STATES.WAIT;
    }

    void finish(int i) {
        if(state == STATES.COOK) {
            state = STATES.WAIT;
            System.out.println("Приготовлен: " + menu[i]);
            System.out.println("Ваша сдача: " + (cash -= prices[i]));
            cash = 0;
            printState();
        }
    }

    void givebackmoney() {
        state = STATES.WAIT;
        System.out.println("Заберите деньги: " + cash);
        cash = 0;
    }
}
