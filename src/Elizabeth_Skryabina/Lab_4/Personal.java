package StaffDemo;

public class Personal extends Employee implements WorkTime {
    int base;

    public Personal(int new_id, String new_name) {
        super(new_id, new_name);
    }

    @Override
    public int pay_work_time(int worktime, int base) {
        return (worktime * base);
    }

    void pay() {
        setPayment(pay_work_time(getWorktime(), base));
    }
}

class Cleaner extends Personal {

    public Cleaner(int new_id, String new_name, int cleaner_worktime) {
        super(new_id, new_name);
        setWorktime(cleaner_worktime);
    }

    public void setBonus_cleaner(int BASE_CLEANER) {
        base = BASE_CLEANER;
    }
}

class Driver extends Personal {
    public Driver(int new_id, String new_name, int driver_worktime) {
        super(new_id, new_name);
        setWorktime(driver_worktime);
    }

    public void setBonus_driver(int BASE_DRIVER) {
        base = BASE_DRIVER;
    }
}